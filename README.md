# LibASM

The purpose of this project is assembly discovery.

Nowadays, C compilers are so advance that they optimise them self very well.

Moreover, machines most of developpments nowadays are targetting hardware that do not require devlopment at the level of assembly (C is enough).

# Description

This project implements some libC functions with x86 assembly. It is developped with NASM compiler >= 2.11.

The projects targets linux 64bits and osx 64bits.

## Build

Build requires **nasm >= 2.11**.
Build generates static library `libs.a` into `./build` directory.

## Test

Use `make test` to build test binarys. They are provided into `./build/` and start all wth `test_`.

## GDB

Example on how to use GDB to debug library.

```
gdb
set environment LD_LIBRARY_PATH=./build # Only for shared librarys
set disassembly-flavor intel            # Set syntax as x86 assembly
file test_X                             # Debug the file test_X
run
b <function1>                           # Set a breakpoint on <function1> 
...
b <function1>
run
c                                        # Keep running to the next breakpoint
c
info registers rdi rsi rax rbx rcx       # Gives infos on bunch of registers
make re                                 # You can direclty run shell commands from GDB
...
```

## LLDB

Example on how to use LLDB to debug library on osx.
All LLDB doc is [here](https://lldb.llvm.org/use/map.html).

```
lldb
settings set target.x86-disassembly-flavor intel            # Set syntax as x86 assembly
file test_X                             # Debug the file test_X
run
b <function1>                           # Set a breakpoint on <function1> 
...
b <function1>
run
c                                        # Keep running to the next breakpoint
c
register read rdi rsi rax rbx rcx       # Gives infos on bunch of registers
platform shell make re                  # Run shell command with platform shell <command> <param1> <param2> ...
...
```

# Some sources to start bequem with assembly

- [Functions tack and parameters](https://en.wikibooks.org/wiki/X86_Disassembly/Functions_and_Stack_Frames)