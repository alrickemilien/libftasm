# Set default C compiler
ifeq ($(CC),)
CC=/usr/bin/clang
endif

# Set default asm compiler
ifeq ($(CCASM),)
CCASM=/usr/bin/nasm
endif

OUT_DIR=./build

$(shell mkdir -p $(OUT_DIR))

include make/libasm.mk
include make/test.mk

.PHONY: all clean fclean test

all: $(LIBASM) test

clean:
	@rm -rf $(OBJ)

fclean: clean
	@rm -rf $(LIBASM) $(TESTS)

re: fclean all
