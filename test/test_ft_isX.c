#include <ctype.h>
#include "libasm.h"
#include "unittest.h"

UNITTEST(test_ft_isalpha)
{
    char c;

    // printf("c:%d - test_ft_isalpha(c++):%d\n", c, test_ft_isalpha(c));

    c = 0;
    c = 'A';
    while (c < 'A')
        assert(ft_isalpha(c++) == 0);
    while (c <= 'Z')
        assert(ft_isalpha(c++) == 1);
    while (c < 'a')
        assert(ft_isalpha(c++) == 0);
    while (c <= 'z')
        assert(ft_isalpha(c++) == 1);
    while (c != 0)
        assert(ft_isalpha(c++) == 0);
}

UNITTEST(test_ft_isdigit)
{
    char c;

    c = 0;
    // printf("c:%d - ft_isdigit(c++):%d\n", c, ft_isdigit(c));

    while (c < '0')
        assert(ft_isdigit(c++) == 0);
    while (c <= '9')
        assert(ft_isdigit(c++) == 1);
    while (c != 0)
        assert(ft_isdigit(c++) == 0);
}

UNITTEST(test_ft_isascii)
{
    char c;

    c = 0;
    while (c >= 0)
        assert(ft_isascii(c++) == 1);
    while (c != 0)
        assert(ft_isascii(c++) == 0);
}

UNITTEST(test_ft_isprint)
{
    char c;

    c = 0;
    while (c < ' ')
        assert(ft_isprint(c++) == 0);
    while (c <= '~')
        assert(ft_isprint(c++) == 1);
    while (c != -1)
        assert(ft_isprint(c++) == 0);
}

UNITTEST(test_ft_isalnum)
{
    char c;

    c = 0;
    while (c < '0')
        assert(ft_isalnum(c++) == 0);
    while (c <= '9')
        assert(ft_isalnum(c++) == 1);
    while (c < 'A')
        assert(ft_isalnum(c++) == 0);
    while (c <= 'Z')
        assert(ft_isalnum(c++) == 1);
    while (c < 'a')
        assert(ft_isalnum(c++) == 0);
    while (c <= 'z')
        assert(ft_isalnum(c++) == 1);
    while (c != (char)(-1))
        assert(ft_isalnum(c++) == 0);
}

int main(void)
{
    test_ft_isalpha();
    test_ft_isdigit();
    test_ft_isascii();
    test_ft_isprint();
    test_ft_isalnum();
}