#include "libasm.h"
#include "unittest.h"

UNITTEST(test_ft_puts)
{
    ft_puts("anticonstitutionnelement");
    ft_puts("Next string is null string");
    ft_puts(0);
    ft_puts("Next string is empty string");
    ft_puts("");
    ft_puts("Next string is only \\n character");
    ft_puts("\n");
    ft_puts("Next string is only \\x03 character");
    ft_puts("\x03");
}

int main(void)
{
    test_ft_puts();
}