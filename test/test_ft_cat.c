#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "libasm.h"

int main(int argc, char **argv)
{
	int fd;

	if (argc == 2)
	{
		if ((fd = open(argv[1], O_RDONLY, 0)) < 0) 
			return (1);
		ft_cat(fd);
	}
	else
		ft_cat(0);
	return (0);
}