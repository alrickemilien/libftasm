#include "libasm.h"
#include "unittest.h"

UNITTEST(test_ft_toupper)
{
    char c;

    c = 0;
    while (c < 'A')
        assert(ft_isalpha(c++) == 0);
    while (c <= 'Z')
        assert(ft_isalpha(c++) == 1);
    while (c < 'a')
        assert(ft_isalpha(c++) == 0);
    while (c <= 'z')
        assert(ft_isalpha(c++) == 1);
    while (c != 0)
        assert(ft_isalpha(c++) == 0);
}

UNITTEST(test_ft_tolower)
{
    char c;

    c = 0;
    while (c < 'A')
        assert(ft_isalpha(c++) == 0);
    while (c <= 'Z')
        assert(ft_isalpha(c++) == 1);
    while (c < 'a')
        assert(ft_isalpha(c++) == 0);
    while (c <= 'z')
        assert(ft_isalpha(c++) == 1);
    while (c != 0)
        assert(ft_isalpha(c++) == 0);
}

int main(void)
{
    test_ft_toupper();
    test_ft_tolower();
}