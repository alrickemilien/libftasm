#include <string.h>
#include <stdint.h>
#include "libasm.h"
#include "unittest.h"

UNITTEST(test_ft_memset)
{
    uint8_t    src[250];
    uint8_t    dest[250];

    assert(ft_memset(src, 0, 250 * sizeof(uint8_t)) == src);
    assert(ft_memset(dest, 0, 250 * sizeof(uint8_t)) == dest);
    assert(memcmp(dest, src, 250 * sizeof(uint8_t)) == 0);

    ft_memset(dest, 'X', 20 * sizeof(uint8_t));
    ft_memset(src, 'X', 20 * sizeof(uint8_t));
    assert(memcmp(dest, src, 20 * sizeof(uint8_t)) == 0);
    assert(memcmp(dest + 21, src + 21, 21 * sizeof(uint8_t)) == 0);

    assert(ft_memset(src, 'X', 0) == src);
    assert(ft_memset(dest, 'X', 0) == dest);
    assert(memcmp(dest, src, 20 * sizeof(uint8_t)) == 0);
    assert(memcmp(dest + 21, src + 21, 21 * sizeof(uint8_t)) == 0);

    ft_memset(dest, 'A', 20 * sizeof(uint8_t));
    ft_memset(src, 'X', 20 * sizeof(uint8_t));
    assert(memcmp(dest, src, 20 * sizeof(uint8_t)) < 0);

    ft_memset(dest, 'Z', 20 * sizeof(uint8_t));
    ft_memset(src, 'E', 20 * sizeof(uint8_t));
    assert(memcmp(dest, src, 20 * sizeof(uint8_t)) > 0);
}

UNITTEST(test_ft_memcpy)
{
    uint8_t    src[250];
    uint8_t    dest[250];

    memset(src, 0, 250 * sizeof(uint8_t));
    assert(ft_memcpy(dest, src, 250 * sizeof(uint8_t)) == dest);
    assert(memcmp(dest, src, 250 * sizeof(uint8_t)) == 0);

    memset(dest, 'X', 20 * sizeof(uint8_t));
    ft_memcpy(src, dest, 20 * sizeof(uint8_t));
    assert(memcmp(dest, src, 20 * sizeof(uint8_t)) == 0);
    assert(memcmp(dest + 21, src + 21, 21 * sizeof(uint8_t)) == 0);

    assert(ft_memcpy(dest, src, 0) == dest);
    assert(memcmp(dest, src, 20 * sizeof(uint8_t)) == 0);    
    assert(memcmp(dest + 21, src + 21, 21 * sizeof(uint8_t)) == 0);

    assert(ft_memcpy(dest + 10, src, sizeof(uint8_t) * 20) == dest + 10);
    assert(memcmp(dest, src, 30 * sizeof(uint8_t)) > 0);
}
UNITTEST(test_ft_bzero)
{
    uint8_t    src[250];
    uint8_t    dest[250];

    memset(src, 'X', 250 * sizeof(uint8_t));
    memset(dest, 0, 250 * sizeof(uint8_t));
    ft_bzero(src, 250 * sizeof(uint8_t));
    assert(memcmp(dest, src, 250 * sizeof(uint8_t)) == 0);

    memset(src + 50, 'X', 50 * sizeof(uint8_t));
    memset(dest, 0, 250 * sizeof(uint8_t));
    ft_bzero(src, 250 * sizeof(uint8_t));
    assert(memcmp(dest, src, 250 * sizeof(uint8_t)) == 0);
}

UNITTEST(test_ft_memchr)
{
    char    dest[250];
    char    *ptr;

    memset(dest, 'X', sizeof(char) * 250);
    dest[249] = 0;
    dest[70] = 'O';
    dest[72] = 'O';
    dest[74] = 'O';
    assert((ptr = ft_memchr(dest, 'O', 250)) == dest + 70);

    memset(dest, 0, sizeof(char) * 250);
    dest[70] = 'O';
    assert((ptr = ft_memchr(dest, 'O', 250)) == dest + 70);

    memset(dest, 'X', sizeof(char) * 42);
    dest[42] = 0;
    dest[70] = 'O';
    assert((ptr = ft_memchr(dest, 'O', 250)) == dest + 70);

    memset(dest, 'X', sizeof(char) * 250);
    assert((ptr = ft_memchr(dest, 'X', 250)) == dest);

    memset(dest, 'X', sizeof(char) * 250);
    dest[248] = 'O';
    assert((ptr = ft_memchr(dest, 'O', 250)) == dest + 248);
}

UNITTEST(test_ft_memrchr)
{
    char    dest[250];
    char    *ptr;

    memset(dest, 'X', sizeof(char) * 250);
    dest[249] = 0;
    dest[70] = 'O';
    dest[72] = 'O';
    dest[74] = 'O';
    assert((ptr = ft_memrchr(dest, 'O', 250)) == dest + 74);

    memset(dest, 0, sizeof(char) * 250);
    dest[70] = 'O';
    assert((ptr = ft_memrchr(dest, 'O', 250)) == dest + 70);

    memset(dest, 'X', sizeof(char) * 42);
    dest[42] = 0;
    dest[70] = 'O';
    assert((ptr = ft_memrchr(dest, 'O', 255)) == dest + 70);

    memset(dest, 'X', sizeof(char) * 250);
    assert((ptr = ft_memrchr(dest, 'X', 250)) == dest + 249);

    memset(dest, 'X', sizeof(char) * 250);
    dest[248] = 'O';
    assert((ptr = ft_memrchr(dest, 'O', 250)) == dest + 248);
}

int main(void)
{
    test_ft_memset();
    test_ft_memcpy();
    test_ft_bzero();
    test_ft_memchr();
    test_ft_memrchr();
}
