#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "libasm.h"
#include "unittest.h"

UNITTEST(test_ft_strlen)
{
    char    s[256];

    assert(ft_strlen("") == 0);
    assert(ft_strlen("\0j\n0") == 0);
    assert(ft_strlen("\n\0") == 1);

    memset(s, 0, 256 * sizeof(uint8_t));

    memset(s, 'X', 55);
    assert(ft_strlen(s) == 55);
    
    memset(s, 'X', 255);
    assert(ft_strlen(s) == 255); 
}

UNITTEST(test_ft_strcat)
{
    char    src[250];
    char    dest[250];

    memset(src, 0, 250 * sizeof(char));
    memset(dest, 0, 250 * sizeof(char));

    assert(ft_strcat(dest, src) == dest);
    assert(strlen(dest) == 0);
    assert(memcmp(dest, src, 250 * sizeof(char)) == 0);

    memset(dest, 'X', 20);
    assert(ft_strcat(dest, src) == dest);
    assert(strlen(dest) == 20);
    assert(memcmp(dest + 20, src + 20, 230 * sizeof(char)) == 0);

    memset(src, 'X', 20);
    assert(ft_strcat(dest, src) == dest);
    assert(strlen(dest) == 40);
    assert(memcmp(dest + 40, src + 40, 210 * sizeof(char)) == 0);
}

UNITTEST(test_ft_strdup)
{
    char    src[250];
    char    *dup;

    memset(src, 0, 250 * sizeof(char));

    assert((dup = ft_strdup(src)) != src);
    assert(strlen(dup) == 0);
    assert(memcmp(dup, src, sizeof(char)) == 0);
    free(dup);

    assert((dup = ft_strdup("Hello!")) != src);
    assert(strlen(dup) == 6);
    assert(memcmp(dup, "Hello!", (6 + 1) * sizeof(char)) == 0);
    free(dup);

    assert((dup = ft_strdup("Hello!\0lalala")) != src);
    assert(strlen(dup) == 6);
    assert(memcmp(dup, "Hello!", (6 + 1) * sizeof(char)) == 0);
    free(dup);

    assert((dup = ft_strdup("")) != src);
    assert(strlen(dup) == 0);
    assert(memcmp(dup, "", sizeof(char)) == 0);
    free(dup);
}

UNITTEST(test_ft_strchr)
{
    char    dest[250];
    char    *ptr;

    memset(dest, 'X', sizeof(char) * 250);
    dest[249] = 0;
    dest[70] = 'O';
    dest[72] = 'O';
    dest[74] = 'O';
    assert((ptr = ft_strchr(dest, 'O')) == dest + 70);

    memset(dest, 0, sizeof(char) * 250);
    dest[70] = 'O';
    assert((ptr = ft_strchr(dest, 'O')) == 0);

    memset(dest, 'X', sizeof(char) * 42);
    dest[42] = 0;
    dest[70] = 'O';
    assert((ptr = ft_strchr(dest, 'O')) == 0);

    memset(dest, 'X', sizeof(char) * 250);
    dest[249] = 0;
    assert((ptr = ft_strchr(dest, 'X')) == dest);

    memset(dest, 'X', sizeof(char) * 250);
    dest[248] = 'O';
    dest[249] = 0;
    assert((ptr = ft_strchr(dest, 'O')) == dest + 248);
}

UNITTEST(test_ft_strrchr)
{
    char    dest[250];
    char    *ptr;

    // printf("(ptr = ft_strchr(dest, 'O')): %p - dest: %p\n", (ptr = ft_strrchr(dest, 'O')), dest);

    memset(dest, 'X', sizeof(char) * 250);
    dest[249] = 0;
    dest[70] = 'O';
    dest[72] = 'O';
    dest[74] = 'O';
    assert((ptr = ft_strrchr(dest, 'O')) == dest + 74);

    memset(dest, 0, sizeof(char) * 250);
    dest[70] = 'O';
    assert((ptr = ft_strrchr(dest, 'O')) == 0);

    memset(dest, 'X', sizeof(char) * 42);
    dest[42] = 0;
    dest[70] = 'O';
    assert((ptr = ft_strrchr(dest, 'O')) == 0);
    
    memset(dest, 'X', sizeof(char) * 250);
    dest[249] = 0;
    assert((ptr = ft_strrchr(dest, 'X')) == dest + 248);

    memset(dest, 'X', sizeof(char) * 250);
    dest[248] = 'O';
    dest[249] = 0;
    assert((ptr = ft_strrchr(dest, 'O')) == dest + 248);
}

UNITTEST(test_ft_strncat)
{
    char    src[250];
    char    dest[250];

    memset(src, 0, 250 * sizeof(char));
    memset(dest, 0, 250 * sizeof(char));
    assert(ft_strncat(dest, src, 20) == dest);
    assert(strlen(dest) == 0);
    assert(memcmp(dest, src, 250 * sizeof(char)) == 0);

    memset(dest, 'X', 20);
    assert(ft_strncat(dest, src, 5) == dest);
    assert(strlen(dest) == 20);
    assert(memcmp(dest + 20, src + 20, 230 * sizeof(char)) == 0);

    memset(dest, 0, 250 * sizeof(char));
    memset(src, 0, 250 * sizeof(char));
    dest[249] = 0;
    src[249] = 0;
    memset(src, 'X', 20);
    // Append only 5 character of src to dest
    assert(ft_strncat(dest, src, 5) == dest);
    assert(strlen(dest) == 5);
    assert(memcmp(dest + 5, src + 20, 50 * sizeof(char)) == 0);
}

UNITTEST(test_ft_strpbrk)
{
    char    src[250];

    memset(src, 0, 250 * sizeof(char));
    assert(ft_strpbrk(src, "ABCDEFG") == 0);

    src[0] = 'A';
    src[25] = 'Z';
    assert(ft_strpbrk(src, "XYZ") == 0);

    assert(ft_strpbrk(src, "") == 0);

    src[0] = 'A';
    src[25] = 'Z';
    assert(ft_strpbrk(src, "XYZ") == 0);

    memset(src, 'X', 20);
    src[5] = 'Z';
    src[6] = 'Y';
    assert(ft_strpbrk(src, "ZY") == src + 5);
    assert(ft_strpbrk(src, "YZ") == src + 5);

    src[5] = 'Y';
    src[6] = 'Z';
    assert(ft_strpbrk(src, "ZY") == src + 5);
    assert(ft_strpbrk(src, "YZ") == src + 5);
}


int main(void)
{
    test_ft_strlen();
    test_ft_strcat();
    test_ft_strdup();
    test_ft_strchr();
    test_ft_strrchr();
    test_ft_strncat();
    test_ft_strpbrk();
}
