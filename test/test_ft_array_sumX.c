#include "libasm.h"
#include "unittest.h"

UNITTEST(test_ft_array_sum32)
{
    int a[] = { 0, 5, 10, 15, 20, 25 };

    assert(ft_array_sum32(a, 6) == 75);

    int b[] = { 0, 5, 10, 15, 20, 25 };
    assert(ft_array_sum32(b, 2) == 5);

    int c[] = { 0, 0, 0, 0, 0, 0 };
    assert(ft_array_sum32(c, 6) == 0);

    int d[] = {  };
    assert(ft_array_sum32(d, 0) == 0);

    int e[] = { -15, -10, -5, 0, 5, 10 };
    assert(ft_array_sum32(e, 6) == -15);
}

int main(void)
{
    test_ft_array_sum32();
}
