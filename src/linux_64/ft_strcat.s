section .text
global _ft_strcat:function
extern _ft_strlen
extern _ft_memcpy

_ft_strcat: ; char *ft_strcat(char *s1, const char *s2)
	push	rbp
	mov		rbp, rsp

	push	rdi
	push	rsi

	mov		rdi, rsi

	call	_ft_strlen

	mov		rdx, rax       ;len rsi
	inc		rdx            ;copy the zero too

	mov		rdi, [rsp + 8] ;get original rdi

	call	_ft_strlen    ;move rdi to end of string

	pop		rsi
	mov		rdi, [rsp]
	add		rdi, rax
	sub		rsp, 8         ;padding

	call	_ft_memcpy

	add		rsp, 8         ;remove padding
	pop		rax

	leave
	ret
