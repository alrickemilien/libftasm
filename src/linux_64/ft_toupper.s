section .text
global _ft_toupper:function

_ft_toupper: ; int ft_toupper(int c)
	push	rbp
	mov		rbp, rsp

	mov		rax, rdi

	cmp		rdi, 97
	jl		_ft_toupper_leave
	cmp		rdi, 122
	jg		_ft_toupper_leave

	sub		rax, 32				; stores uppercase into RAX return register
_ft_toupper_leave:
	leave
	ret
