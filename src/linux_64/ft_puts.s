section .data
linebreak db 10
null db "(null)", 0
null_length equ $ - null

section .text
global _ft_puts:function

_ft_puts:
	push	rbp					; prolog
	mov		rbp, rsp
	xor		rbx, rbx
	cmp		rdi, rbx
	je		_ft_puts_zero
	mov		r12, rdi			; put rdi into rd12 because rdi wiil be use by write syscall
_ft_puts_loop:
	cmp		byte [r12 + rbx], 0	; check null termination
	je		_ft_puts_newline
	mov		rdi, 1
	lea		rsi, [r12 + rbx]
	mov		rdx, 1
	mov		rax, 0x1
	syscall
	inc		rbx
	jmp		_ft_puts_loop
_ft_puts_newline:
	mov		rdi, 1
	lea		rsi, [rel linebreak]
	mov		rdx, 1
	mov		rax, 0x1
	syscall
	jmp		_ft_puts_leave
_ft_puts_zero:
	mov		rdi, 1
	lea		rsi, [rel null]
	mov		rdx, null_length
	mov		rax, 0x1
	syscall
	jmp		_ft_puts_newline
_ft_puts_leave:
	leave
	ret
