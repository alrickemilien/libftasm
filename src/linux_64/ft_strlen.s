section .text
global _ft_strlen:function

_ft_strlen: ; size_t ft_strlen(const char *s)
	push	rbp				; prolog
	mov		rbp, rsp
	mov		r8, rdi			; save RDI
	cmp		rdi, 0			; test if RDI is null
	je		_ft_strlen_leave
	mov		rcx, -1			; set RCX to maximal bit value
	mov		al, 0			; set the char to research
	cld						; init direction flag
	repne	scasb			; start the research on the RDI register
	not		rcx				; reverse RCX
	lea		rax, [rcx - 1]	; get value of RCX without the \0 count
	mov		rdi, r8			; reset RDI which was auto increment by repne scasbi
_ft_strlen_leave:
	leave
	ret