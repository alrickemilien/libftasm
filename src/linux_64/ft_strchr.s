section .text
global _ft_strchr:function

_ft_strchr: ; char *ft_strchr(const char *s, int c)
	mov rax, rdi
	jmp _ft_strchr_start
_ft_strchr_loop:
	inc rax
	test dl, dl
	je _ft_strchr_return_0
_ft_strchr_start:
	movzx edx, byte [rax]
	cmp dl, sil
	jne _ft_strchr_loop
	ret
_ft_strchr_return_0:
	xor eax, eax
	ret
	
	