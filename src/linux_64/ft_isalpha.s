section .text
global _ft_isalpha:function

_ft_isalpha: ; int ft_isalpha(int c)
	push	rbp
	mov		rbp, rsp
	cmp		rdi, 65
	jl		_ft_isalpha_false
	cmp		rdi, 90
	jle		_ft_isalpha_true
	cmp		rdi, 97
	jl		_ft_isalpha_false
	cmp		rdi, 122
	jle		_ft_isalpha_true
_ft_isalpha_false:
	mov		rax, 0
	leave
	ret
_ft_isalpha_true:
	mov		rax, 1
	leave
	ret
