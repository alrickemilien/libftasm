section .data						; global constant set into data section
buffer_size equ 512

section .bss						; global variable set into bss section
buffer resb buffer_size

section .text
global _ft_cat:function
extern _ft_strlen
extern _ft_memcpy

_ft_cat: ; ft_cat(const int fd)
	push	rbp						; prolog
	mov		rbp, rsp

	mov		rbx, rdi

_ft_cat_loop:
	mov		rdi, rbx				; fd
	lea		rsi, [rel buffer]		; buffer
	mov		rdx, buffer_size		; read size
	dec		rdx
	mov		rax, 0x0				; sys_read
	syscall
_dbg:
	jc		_ft_cat_leave			; quit if read set carry (error)
	cmp		rax, 0					; check for end of file
	jle		_ft_cat_leave

	mov		rdi, 0x1				; stdout
	lea		rsi, [rel buffer]		; buffer
	mov		rdx, rax                ; length returned by read
	mov		rax, 0x1				; sys_write
	syscall

	jmp _ft_cat_loop
_ft_cat_leave:
	leave
	ret