section .text
global _ft_memcpy:function

_ft_memcpy: ; void *ft_memcpy(void *dst, const void *src, size_t n)
	push	rbp
	mov		rbp, rsp

	mov		rax, rdi
	mov		rcx, rdx

	rep		movsb

	leave
	ret
