section .text
global _ft_strdup:function
extern _ft_strlen
extern _ft_memcpy
extern _ft_bzero
extern malloc

_ft_strdup: ; char *ft_strdup(const char *s1)
	push	rbp				; prolog
	mov		rbp, rsp

	cmp		rdi, 0			; null string check
	je		_ft_strdup_fail
	mov		r13, rdi		; src pointeur is stored into R13
	call	_ft_strlen
	inc		rax				; increment length stored into rax
	mov		r9, rax			; src length is stored into R9
	mov		rdi, r9			; dest is stored into R9
	push	r9				; preserve src length R9 on the stack
	push	r13				; preserve R13 register
	call	_malloc
	pop		r13				; recover R13 register
	pop		r9				; recover src length
	cmp		rax, 0			; checl null malloc return
	je		_ft_strdup_fail
	mov		r8, rax			; stores malloc pointeur into R8, because later bzero call will whipe RAX
	mov		rdi, r8			; stores malloc pointeur into RDI, for bzero access
	mov		rsi, r9			; stores src length into RSI
	call	_ft_bzero
	dec		r9 				; length-- src fto return a 0 terminated string
	push	r8				; preserve R8 register
	mov		rdi, r8			; prepare memcpy parameters
	mov		rsi, r13
	mov		rdx, r9
	call	_ft_memcpy
	pop		rax				; recover R8 register allocated pointeur
	leave
	ret

_ft_strdup_fail:
	xor		rax, rax		; return 0
	leave
	ret


