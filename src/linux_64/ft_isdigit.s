extern  _GLOBAL_OFFSET_TABLE_  ; Each code module in your shared library should define the GOT as an external symbol
global _ft_isdigit:function

section .text
_ft_isdigit: ; int ft_isdigit(int c)
	push	rbp
	mov		rbp, rsp
	cmp		rdi, 48
	jl		_ft_isdigit_false
	cmp		rdi, 57
	jg		_ft_isdigit_false
	mov		rax, 1
	leave
	ret
_ft_isdigit_false:
	mov		rax, 0
	leave
	ret
