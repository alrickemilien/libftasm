section .text
global _ft_memset:function

_ft_memset: ; void *ft_memset(void *b, int c, size_t len)
	push	rbp
	mov		rbp, rsp
	mov		r8, rdi ; save rdi
	mov		rax, rsi ; mov len
	mov		rcx, rdx ; mov char to set
	cld
	rep		stosb
	mov		rax, r8
	leave
	ret


