section .text
global _ft_tolower:function

_ft_tolower: ; int ft_tolower(int c)
	push	rbp
	mov		rbp, rsp

	mov		rax, rdi

	cmp		rdi, 65
	jl		_ft_tolower_leave
	cmp		rdi, 90
	jg		_ft_tolower_leave

	add		rax, 32				; stores lowercase into RAX return register
_ft_tolower_leave:
	leave
	ret
