section .text
global _ft_bzero:function

_ft_bzero: ; ft_bzero(char *s)
	push rbp		; prolog
	mov rbp, rsp
	mov rcx, rsi
	xor al, al
	rep stosb
	leave
	ret


