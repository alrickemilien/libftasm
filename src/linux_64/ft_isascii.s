section .text
global _ft_isascii:function

_ft_isascii: ; int ft_isascii(int c)
	push	rbp
	mov		rbp, rsp
	cmp		rdi, 0
	jl		_ft_isascii_false
	cmp		rdi, 127
	ja		_ft_isascii_false
	mov		rax, 1
	leave
	ret
_ft_isascii_false:
	mov		rax, 0
	leave
	ret
