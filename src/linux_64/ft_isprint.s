section .text
global _ft_isprint:function

_ft_isprint: ; int ft_isprint(int c)
	push	rbp
	mov		rbp, rsp
	cmp		rdi, 32
	jl		_ft_isprint_false
	cmp		rdi, 126
	ja		_ft_isprint_false
	mov		rax, 1
	leave
	ret
_ft_isprint_false:
	mov rax, 0
	leave
	ret
