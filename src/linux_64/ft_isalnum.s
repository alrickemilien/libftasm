section .text
global _ft_isalnum:function
extern _ft_isdigit
extern _ft_isalpha

_ft_isalnum: ; int ft_isalnum(int c)
	push	rbp
	mov		rbp, rsp
	mov		rdx, 0
	call	_ft_isdigit
	add		rdx, rax
	call	_ft_isalpha
	add		rdx, rax
	mov		rax, rdx
	leave
	ret