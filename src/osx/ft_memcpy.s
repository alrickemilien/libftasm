section .text
global _ft_memcpy

_ft_memcpy: ; void *ft_memcpy(void *dst, const void *src, size_t n)
	enter	8, 0

	mov		rax, rdi
	mov		rcx, rdx

	rep		movsb

	leave
	ret
