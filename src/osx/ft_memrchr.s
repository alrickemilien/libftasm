section .text
global _ft_memrchr

_ft_memrchr: ; char *ft_memrchr(const char *s, int c, size_t n)
	dec     rdx
	movsxd  rdx, edx
	add     rdi, rdx
	test    rdx, rdx
	js      _ft_memrchr_leave_0
_ft_memrchr_loop:
	mov     cl, [rdi]
	cmp     cl, sil
	je      _ft_memrchr_leave
	
	dec     rdi
	dec     rdx
	jns     _ft_memrchr_loop
_ft_memrchr_leave_0:
	xor     eax, eax
	ret
_ft_memrchr_leave:
	mov     rax, rdi
	ret