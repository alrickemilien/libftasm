section .text
global _ft_bzero

_ft_bzero: ; ft_bzero(char *s)
	enter	8, 0		; prolog
	mov		rcx, rsi
	xor		al, al
	rep		stosb
	leave
	ret


