section .text
global _ft_isprint

_ft_isprint: ; int ft_isprint(int c)
	enter	8, 0
	cmp		rdi, 32
	jl		_ft_isprint_false
	cmp		rdi, 126
	ja		_ft_isprint_false
	mov		rax, 1
	leave
	ret
_ft_isprint_false:
	mov rax, 0
	leave
	ret
