section .text
global _ft_tolower

_ft_tolower: ; int ft_tolower(int c)
	enter	8, 0

	mov		rax, rdi

	cmp		rdi, 65
	jl		_ft_tolower_leave
	cmp		rdi, 90
	jg		_ft_tolower_leave

	add		rax, 32				; stores lowercase into RAX return register
_ft_tolower_leave:
	leave
	ret

