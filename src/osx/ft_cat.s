section .data			; global constant set into data section
buffer_size equ 512

section .bss			; global variable set into bss section
buffer resb buffer_size

section .text
global _ft_cat
extern _ft_strlen
extern _ft_memcpy

_ft_cat: ; ft_cat(const int fd)
	enter	8, 0					; prolog
	mov		rbx, rdi				; RBX now contains file descriptor fd
_ft_cat_loop:
	mov		rdi, rbx				; write on file descriptor
	lea		rsi, [rel buffer]
	mov		rdx, buffer_size
	dec		rdx
	mov		rax, 0x2000003
	syscall
_dbg:
	jc		_ft_cat_leave			; quit if read set carry (error)
	cmp		rax, 0					; check end of file
	jle		_ft_cat_leave

	mov		rdi, 0x1				; write on stdout
	lea		rsi, [rel buffer]
	mov		rdx, rax
	mov		rax, 0x2000004
	syscall

	jmp _ft_cat_loop
_ft_cat_leave:
	leave
	ret