section .text
global _ft_strrchr

_ft_strrchr: ; char *ft_strrchr(const char *s, int c)
	enter	8, 0						; prolog
	xor		r10, r10					; index will be stored into R10
_ft_strrchr_to_end_string:		
	cmp		byte[rdi + r10], 0			; check empty string
	je		_ft_strrchr_loop
	inc		r10							; move index forward
	jmp		_ft_strrchr_to_end_string
_ft_strrchr_loop:
	cmp		r10, -1						; check index for maximum size
	je		_ft_strrchr_not_found
	cmp		byte[rdi + r10], sil
	je		_ft_strrchr_found
	dec		r10					 		; move index forward
	jmp		_ft_strrchr_loop
_ft_strrchr_found:
	add		rdi, r10					; RDI now contains adress of found character into the string
	mov		rax, rdi					; stores pointer into RAX return register
	jmp		_ft_strrchr_leave	
_ft_strrchr_not_found:
	mov		rax, 0						; stores NULL into RAX return register
_ft_strrchr_leave:
	leave		
	ret
	