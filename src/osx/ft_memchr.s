section .text
global _ft_memchr

_ft_memchr:	; char *ft_memchr(const char *s, int c, size_t n)
	dec     rdx
	cmp     rdx, -1
	je      _ft_memchr_return_0
_ft_memchr_loop:
	mov     cl, [rdi]
	cmp     cl, sil
	je      _ft_memchr_return
	
	dec     rdx
	inc     rdi
	cmp     rdx, -1
	jne     _ft_memchr_loop
_ft_memchr_return_0:
	xor     eax, eax
	ret
_ft_memchr_return:
	mov     rax, rdi
	ret