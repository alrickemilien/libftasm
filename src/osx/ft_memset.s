section .text
global _ft_memset

_ft_memset: ; void *ft_memset(void *b, int c, size_t len)
	enter	8, 0
	mov		r8, rdi		; R8 contains now pointeur to buffer
	mov		rax, rsi	; RAX contains now the character
	mov		rcx, rdx	; RCX contains now length
	cld					; 
	rep		stosb		; 
	mov		rax, r8		; put the adress of the buffer into RAX return register
	leave
	ret


