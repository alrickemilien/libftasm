section .text
global _ft_strpbrk

_ft_strpbrk: ; char *ft_strpbrk(const char *s, const char *charset)
    enter   8, 0
	xor     rcx, rcx                   ; RCX will be the index
_ft_strpbrk_loop:                      ; loop on te string
    cmp     byte[rdi + rcx], 0
	je      _ft_strpbrk_loop_null
	jmp     _ft_strpbrk_find
_ft_strpbrk_end_charset:		
    inc     rcx
	jmp     _ft_strpbrk_loop
_ft_strpbrk_find:
    xor     r10, r10
	mov     r11b, byte[rdi + rcx]       ; stores the byte into r11b register
_ft_strpbrk_loop_charset:
    cmp     byte[rsi + r10], 0
	je      _ft_strpbrk_end_charset
	cmp     byte[rsi + r10], r11b
	je      _ft_strpbrk_loop_leave
	inc     r10
	jmp     _ft_strpbrk_loop_charset
_ft_strpbrk_loop_null:
    xor     rax, rax
	leave
	ret
_ft_strpbrk_loop_leave:
    add     rcx, rdi
	mov     rax, rcx
    leave
	ret