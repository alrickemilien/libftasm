section .text
global _ft_strncat

; Repeat Prefix		Termination Condition 1	Termination Condition 2
; REP				ECX=0					None
; REPE/REPZ			ECX=0					ZF=0
; REPNE/REPNZ		ECX=0					ZF=1


; x86_64 linux/osx machine passes function parameters in RDI, RSI, RDX, RCX, R8, and R9

_ft_strncat: ; char *ft_strncat(char *dest, const char *src, size_t n)
	or		r10, -1				; set r10 to maximum index size
	xor		rax, rax			; set RAX register to 0
	mov		rcx, r10			; set RCX index to R10 index
	mov		r8, rdi				; set R8 to RDI dest string pointeur, because following scasb moves RDI
	repnz	scasb				; scasb searches the memory for the byte in RAX, starting at RDI, decreasing RCX
								; this moves RDI to the end of dest string because RAX is 0

	mov		rdi, rsi			; set RDI pointing on start of RSI src string
	not		rcx					; notting RCX transorm (max - size of dest string) to (size of dest string)
	lea		r9, [r8 + rcx - 1]	; stores start of dest string + size of string into R9 
	or		rcx, -1				; RCX retrieves maximum index

	repnz	scasb				; scasb searches the memory for the byte in RAX, starting at RDI, decreasing RCX
								; this moves RDI to the end of src string because RAX is 0
	
	mov		rdi, r9				; RDI now contains dest start of dest string + size of string
	mov		rax, r8				; RAX now contains start of dest string
	cmp		rdx, rcx			; compare RDX and RCX, see below
	cmovbe	rcx, rdx			; stores n or length of src if lesser than n into RCX
								; conditional move that stores RDX into RCX
								; only if cmp rdx, rcx tells that RCX lower than RDX

	mov		byte [r9 + rcx], 0	; set null termination on (dest + n )or (dest + length of src)
	rep		movsb				; copy each bytes of RSI (src) into RDI (end of src) RCX times
	
	ret