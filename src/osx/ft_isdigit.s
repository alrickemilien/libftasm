section .text
global _ft_isdigit

_ft_isdigit: ; int ft_isdigit(int c)
	enter	8, 0
	cmp		rdi, 48
	jl		_ft_isdigit_false
	cmp		rdi, 57
	jg		_ft_isdigit_false
	mov		rax, 1
	leave
	ret
_ft_isdigit_false:
	mov		rax, 0
	leave
	ret

