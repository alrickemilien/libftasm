
section .text
global _ft_array_sum32

_ft_array_sum32: ; ft_array_sum32(uint32_t *array, uint64_t size)
	enter	8, 0	    				; prolog

	push    rdi		    				; store array ptr

	push    rsi		    				; store array length

    pop     rbx							; retrieve array length into RCX
	shl		rbx, 2						; multiply by 4
	mov		rcx, rdi
	add		rcx, rbx					; compute address following last array element, now RCX contains last element

	xor     rbx, rbx					; sum variable is RBX
	cmp     rdi, rcx					; check whether all array was processed
	jne     _ft_array_sum_add_elements
	pop     rdi							; retrieve array ptr
	xor		rax, rax		    		; handle null result
	leave
	ret
_ft_array_sum_add_elements:
	movsx 	r9, dword [rdi]				; get current element
	add     rbx, r9						; add next element
	add     rdi, 4						; move array pointer to next element
	cmp     rdi, rcx					; check whether all array was processed
	jne     _ft_array_sum_add_elements
	
	pop     rdi							; retrieve array ptr
	mov		rax, rbx		    		; retrieve sum result
	leave
	ret
