section .text
global _ft_isascii


_ft_isascii: ; int ft_isascii(int c)
	enter	8, 0
	cmp		rdi, 0
	jl		_ft_isascii_false
	cmp		rdi, 127
	ja		_ft_isascii_false
	mov		rax, 1
	leave
	ret
_ft_isascii_false:
	mov		rax, 0
	leave
	ret
