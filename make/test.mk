TEST_SRC=test/test_ft_cat.c \
test/test_ft_isX.c \
test/test_ft_strX.c \
test/test_ft_memX.c \
test/test_ft_putX.c \
test/test_ft_toX.c \
test/test_ft_array_sumX.c

TESTS=$(patsubst test/%.c, build/%, $(TEST_SRC))

CC_FLAGS=-Wall -Wextra -Werror

test: $(TESTS)

$(TESTS): build/%: test/%.c
	$(CC) -o $@ $< -I include/ -L$(OUT_DIR) -lfts $(CC_FLAGS)
