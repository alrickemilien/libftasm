NASM_VER_NUM=$(shell $(CCASM) -v | cut -d' ' -f3)

NASM_VER_MAJOR := $(shell echo $(NASM_VER_NUM) | cut -f1 -d.)
NASM_VER_MINOR := $(shell echo $(NASM_VER_NUM) | cut -f2 -d.)

ifeq ($(shell [ $(NASM_VER_MAJOR) -lt 2 -o $(NASM_VER_MINOR) -lt 11 ] && echo true), true)
$(error the actual version is $(NASM_VER_NUM), version >= $(NASM_VER_MAJOR).$(NASM_VER_MINOR) required ...)
endif

ifeq ($(shell uname -s), Darwin)
SFLAGS=-f macho64
endif
ifeq ($(shell uname -s), Linux)
SFLAGS=-f elf64
endif

ifdef DEBUG
SFLAG_DEBUG=-g
endif

LIBASM=$(OUT_DIR)/libfts.a

ifeq ($(shell uname -s), Darwin)
SRC=src/osx/ft_memcpy.s \
src/osx/ft_memrchr.s \
src/osx/ft_strdup.s \
src/osx/ft_memset.s \
src/osx/ft_strlen.s \
src/osx/ft_tolower.s \
src/osx/ft_isascii.s \
src/osx/ft_strrchr.s \
src/osx/ft_isalnum.s \
src/osx/ft_strncat.s \
src/osx/ft_isprint.s \
src/osx/ft_array_sum32.s \
src/osx/ft_memchr.s \
src/osx/ft_puts.s \
src/osx/ft_bzero.s \
src/osx/ft_strchr.s \
src/osx/ft_isalpha.s \
src/osx/ft_isdigit.s \
src/osx/ft_toupper.s \
src/osx/ft_strcat.s \
src/osx/ft_strpbrk.s \
src/osx/ft_cat.s
endif

ifeq ($(shell uname -s), Linux)
SRC=src/linux_64/ft_isalpha.s \
src/linux_64/ft_memchr.s \
src/linux_64/ft_memset.s \
src/linux_64/ft_puts.s \
src/linux_64/ft_toupper.s \
src/linux_64/ft_isalnum.s \
src/linux_64/ft_array_sum32.s \
src/linux_64/ft_cat.s \
src/linux_64/ft_isprint.s \
src/linux_64/ft_isascii.s \
src/linux_64/ft_strncat.s \
src/linux_64/ft_memrchr.s \
src/linux_64/ft_bzero.s \
src/linux_64/ft_strdup.s \
src/linux_64/ft_strchr.s \
src/linux_64/ft_strrchr.s \
src/linux_64/ft_strcat.s \
src/linux_64/ft_memcpy.s \
src/linux_64/ft_isdigit.s \
src/linux_64/ft_tolower.s \
src/linux_64/ft_strpbrk.s \
src/linux_64/ft_strlen.s
endif

OBJ=$(SRC:.s=.o)

$(LIBASM): $(OBJ)
	ar rc $@ $^

%.o: %.s
	$(CCASM) $(SFLAG_DEBUG) $(SFLAGS) $< -o $@
