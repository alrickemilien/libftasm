#ifndef LIBASM_H
# define LIBASM_H

#include <stddef.h>

// Compat linux
# ifndef __MACH__
# define ft_isdigit(x) _ft_isdigit(x)
# define ft_isalpha(x) _ft_isalpha(x)
# define ft_isalnum(x) _ft_isalnum(x)
# define ft_isascii(x) _ft_isascii(x)
# define ft_isprint(x) _ft_isprint(x)
# define ft_toupper(c) _ft_toupper(c)
# define ft_tolower(c) _ft_tolower(c)
# define ft_bzero(s, n) _ft_bzero(s, n)
# define ft_puts(s) _ft_puts(s)
# define ft_strcat(s1, s2) _ft_strcat(s1, s2)
# define ft_memset(b, c, len) _ft_memset(b, c, len)
# define ft_memcpy(dst, src, n) _ft_memcpy(dst, src, n)
# define ft_strlen(s) _ft_strlen(s)
# define ft_strdup(s1) _ft_strdup(s1)
# define ft_cat(fd) _ft_cat(fd)
# define ft_array_sum32(a, s) _ft_array_sum32(a, s)
# define ft_strpbrk(a, s) _ft_strpbrk(a, s)
# define ft_strchr(s, c) _ft_strchr(s, c)
# define ft_strrchr(s, c) _ft_strrchr(s, c)
# define ft_strncat(d, s, c) _ft_strncat(d, s, c)
# define ft_memchr(d, s, c) _ft_memchr(d, s, c)
# define ft_memrchr(d, s, c) _ft_memrchr(d, s, c)
# define ft_cat(x) _ft_cat(x)
# endif

int		ft_isdigit(int c);
int		ft_isalpha(int c);
int		ft_isalnum(int c);
int		ft_isascii(int c);
int		ft_isprint(int c);

int		ft_toupper(int c);
int		ft_tolower(int c);

void	ft_bzero(void *s, size_t n);
int		ft_puts(const char *s);

char	*ft_strcat(char *s1, const char *s2);

void	*ft_memset(void *b, int c, size_t len);
void	*ft_memcpy(void *dst, const void *src, size_t n);
char    *ft_memchr(const char *s, int c, size_t n);
char    *ft_memrchr(const char *s, int c, size_t n);

size_t	ft_strlen(const char *s);
char	*ft_strdup(const char *s1);
char    *ft_strchr(const char *s, int c);
char    *ft_strrchr(const char *s, int c);
char    *ft_strncat(char *dest, const char *src, size_t n);
char    *ft_strpbrk(const char *s, const char *charset);

void	ft_cat(const int fd);

int     ft_array_sum32(const int *array, int size);

#endif
